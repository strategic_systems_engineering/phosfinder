import os
import sys

sys.path.append(os.getcwd())

import phosfinder

my_dem = phosfinder.Dem('tolstoi_small.tif')

#Performs a depression filling functions on the Dem object's stored raster
#Depression filling based on the Wang Liu algorithm
my_dem.fill_sinks(0.01)


#Creates a Flow Direction raster ussalcking the D8 Method
direction_array = my_dem.flow_direction()

#Creates a Flow Accumulation raster based on the DEM and Flow Directions rasters
accumulation_array = my_dem.flow_accumulation(direction_array)


#Output direction DEM and flow accumulation
my_dem.output_raster(direction_array,"temp_acc.tif")
my_dem.output_raster(accumulation_array,"temp_dir.tif")
