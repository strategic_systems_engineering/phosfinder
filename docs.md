# Phosfinder 

PhosFinder is a geospatial evaluation tool, developed by Strategic Systems Engineering, that estimates nutrient transport for Canadian domains.

## 1 DEM 

DEM or "Dem" is used by PhosFinder to represent digital elevation models. The Dem object is used to represent these DEMs. 

<br />

### 1.1 ``fill_sinks(Double min_slope)`` &#8594; ``None``

Using the Wang & Liu method, fill depressions in the DEM (replacing the elevations within the DEM). Will also produce a watershed delineated array as a by-product. 

<br />

### 1.2 `flow_direction()` &#8594; `2D-array direction_array`

Determine each cell's Flow Direction based on the neighbouring pixel with steepest descent in elevation the steepest descent in elevation
- Direction represented as $2^x$, where $x$ is a number $1 - 8$
- Clockwise starting at NE &#8594; N

<br />

### 1.3 `flow_accumulation(2D-array direction_array)` &#8594; `2D-array flow_accumulation_array`
 At each pixel, determine the number of upstream pixels flow into the current pixel.

<br />

 ### 1.4 `output_raster(2D-array, String filename, **kwargs)` &#8594; `None`
 Create a raster output for any of the 2D-arrays with the given `filename`

<br />

## 2 Raster
Used as a wrapper for 2d-arrays with additional features. 

### 2.1 Constructor `Raster(String filename)` 
Constructor for creating Raster objects. 

<br />

## 3 Vector

### 3.1 `pourpoints(Dem dem, 2D-array accumulation_array, 2D-array accumulation_array, String filename)` &#8594; `None`

With a given set of Flow Direction and Flow Accumulation rasters, creates a multi-point shape-file containing a number of sub-watershed outlets. Assumes a sub-watershed is equal in size to 1/10 of the domain. 

<br />

### 3.2 `subwatersheds(Dem dem, String subwatersheds_filename, String pourpoint_shapefile, 2D-array accumulation_array, 2D-array direction_array)` &#8594; `None`

For a generated Pour-point shape-file (containing point/multi-point geometry), create a MultiPolygon shape-file of sub watersheds within the DEM. 

<br />

## 4 Nutrient Mobility

### 4.1 `upstream_length(Dem dem, 2d-array direction_array, Tuple utm_coor)`  &#8594; `2d-array flow_length_array`

Determines the longest upstream flow length for each pixel of a sub-basin identified by a given outlet.

<br />

### 4.2 `surface_roughness(Dem dem, Raster aci_file)` &#8594; `2D-array roughness_array`

Determines Manning's Roughness Coefficient based on land cover usage. 

<br />

### 4.3 `travel_time_downstream(Dem dem, Raster roughness_raster, 2D-array direction_array, 2D-array accumulation_array, 2D-array flow_length_array, int discharge, Tuple utm_coor)` &#8594; `2D-array roughness_array`

Determines Manning's Roughness Coefficient based on land cover usage. Requires a Discharge metric given in m3/s. 

Determines time in Hours.

<br />

## 5 Nutrient Transport 

### 5.1 `tn_yield(Dem dem, Raster aci_file)` &#8594; `2D-array nitrogen_yield_array`

Determines annual nitrogen yield leaving the landscape based on land cover usage.

Metric is in kg/ha/year

<br />

### 5.2 `tp_yield(Dem dem, Raster aci_file)` &#8594; `2D-array phosphorus_yield_array`

Determines annual phosphorus yield leaving the landscape based on land cover usage.

Metric is in kg/ha/year

<br />

### 5.3 `nutrient_mass(Dem dem,2D-array phosphorus_yield_array, 2D-array travel_time_array, String metric, 2D-array accumulation_array)` &#8594; `2D-array nutrient_mass_array`

Determines the mass of nutrients (either phosphorus or nitrogen) lost annually due to runoff.

Output given in Kilograms

<br />

### 5.4 `nutrient_accumulation(Dem dem,2D-array loss_array,2D-array direction_array,Tuple utm_coor)` &#8594; `2D-array nutrient_accumulation_array`

Calculates each cell's Accumulated Nutrient Mass where each cell routes flow towards a common outlet. 

Output given in Kilograms