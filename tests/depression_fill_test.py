import sys
import os
sys.path.append(os.getcwd()+"/tests")
import phosfinder
import warnings

print("Loading tolstoi_small.tif from tests...")
my_dem = phosfinder.Dem('tests/tolstoi_small.tif')

print("\n")

print("Filling Sinks...")
my_dem.fill_sinks(0.01)

print("\n")

print("Creating direction array...")
direction_array = my_dem.flow_direction()

print("\n")

print("Creating accumulation array...")
accumulation_array = my_dem.flow_accumulation(direction_array)

print("\n")

print("Creating output for direction array as test_direction.tif...")
my_dem.output_raster(direction_array,"tests/outputs/test_direction.tif")

print("\n")

print("Creating output for accumulation array as test_accumulation.tif...")
my_dem.output_raster(accumulation_array,"tests/outputs/test_accumulation.tif")

print("\n")

print("Comparing accumulation output with truth")
accum_test = os.system("gdalcompare.py tests/truth/accumulation.tif tests/outputs/test_accumulation.tif")

print("\n")

print("Comparing direction output with truth")
direc_test = os.system("gdalcompare.py tests/truth/direction.tif tests/outputs/test_direction.tif")

print("\n")

if(direc_test == 0):
    print("Direction output test passed")
else:
    print("Direction Output Mismatched")

if(accum_test == 0):
    print("Flow accumulation output test passed")
else:
   print("Flow accumulation output mismatched")