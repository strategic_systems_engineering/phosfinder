from osgeo import ogr, gdal, osr
import numpy as np


#Sink_Polygons
############################################################################
#   Assuming the DEM has already underwent depression filling, create a Polygon
#   shapefile of all of the depressions
#
#   Be aware that this function may take a lot of RAM depending on the raster's
#   size and the pixel resolution. For similar reasons, this function may create
#   a very large shapefile. To mitigate these problem, we advised using a min_size_area
#   value to, at least, greater than the squared area of a pixel.
#
###########################################################################

def sink_polygons(dem, original_dem_filepath, accumulation_array, sink_poly_filename, min_size_area):

    #Create new empty output shapefile
    drv = ogr.GetDriverByName("ESRI Shapefile")
    sink_poly_ds = drv.CreateDataSource( sink_poly_filename )
    new_srs = osr.SpatialReference()
    new_srs.ImportFromWkt(dem.proj_wkt)
    sink_poly_layer = sink_poly_ds.CreateLayer(sink_poly_filename, srs = new_srs)

    #Creating various Fields for the output shapefile
    area_field = ogr.FieldDefn("FillArea", ogr.OFTInteger)
    sink_poly_layer.CreateField(area_field)

    volume_field = ogr.FieldDefn("FillVolume", ogr.OFTReal)
    sink_poly_layer.CreateField(volume_field)

    depth_field = ogr.FieldDefn("FillDepth", ogr.OFTReal)
    sink_poly_layer.CreateField(depth_field)

    elev_field = ogr.FieldDefn("FillElev", ogr.OFTReal)
    sink_poly_layer.CreateField(elev_field)

    drain_field = ogr.FieldDefn("DrainArea", ogr.OFTInteger)
    sink_poly_layer.CreateField(drain_field)


    original_dem = gdal.Open(original_dem_filepath)
    original_dem_array = original_dem.GetRasterBand(1).ReadAsArray()
    original_dem = None

    #Prepare a sink mask for raster -> polygon conversion
    sink_array = dem.array - original_dem_array
    original_dem_array = None

    sink_mask_array = sink_array > 0
    sink_mask_array.astype(int)

    #Store sink mask array in a Raster Band (temporary in-memory raster file)
    temp_mask_ds = gdal.GetDriverByName('MEM').Create('', dem.array_size[1], dem.array_size[0], 1, gdal.GDT_Byte)
    temp_mask_ds.SetProjection(new_srs.ExportToWkt())
    temp_mask_ds.SetGeoTransform(dem.geo_trans)
    temp_mask_ds.GetRasterBand(1).SetNoDataValue(float(dem.nodata_value))
    temp_mask_ds.GetRasterBand(1).WriteArray(sink_mask_array)
    sink_mask_array = None

    gdal.Polygonize( temp_mask_ds.GetRasterBand(1), temp_mask_ds.GetRasterBand(1), sink_poly_layer, -1, [], callback=None )

    temp_mask_ds = None

    #Make temporary polygon layer to house a single sink polygon
    temp_polygon_ds = ogr.GetDriverByName('MEMORY').CreateDataSource( 'memData' )

    #Calculate and Set attributes to each Sink Polygon
    for polygon_feature in sink_poly_layer:

        #Calculate and set Fill Area field
        polygon_geom = polygon_feature.GetGeometryRef()
        polygon_area = polygon_geom.GetArea()

        if polygon_area >= min_size_area:
            polygon_feature.SetField("FillArea", polygon_area)

            temp_polygon_layer = temp_polygon_ds.CreateLayer('', new_srs, ogr.wkbPolygon)
            temp_polygon_layer.CreateFeature(polygon_feature)

            polygon_volume = 0
            polygon_elev = 0
            polygon_depth = 0
            polygon_drain = 0

            #Find pixels overlayed by a sink polygon
            temp_rasterize_ds = gdal.GetDriverByName('MEM').Create('', dem.array_size[1], dem.array_size[0], 1, gdal.GDT_Byte)
            temp_rasterize_ds.SetProjection(dem.proj_wkt)
            temp_rasterize_ds.SetGeoTransform(dem.geo_trans)
            temp_rasterize_ds.GetRasterBand(1).SetNoDataValue(float(dem.nodata_value))
            gdal.RasterizeLayer(temp_rasterize_ds, [1], temp_polygon_layer, burn_values=[1])
            temp_rasterize_array = temp_rasterize_ds.GetRasterBand(1).ReadAsArray()

            #Investigate each pixel of the depression
            polygon_pixels_Y, polygon_pixels_X = np.nonzero(temp_rasterize_array)
            temp_rasterize_array = None
            for i in range(len(polygon_pixels_Y)):
                row = polygon_pixels_Y[i]
                col = polygon_pixels_X[i]

                #Calculate Fill Volume, Fill Depth, Fill Elev and Drain Area
                polygon_volume += sink_array[row][col] * dem.geo_trans[1] * (-1*dem.geo_trans[5])

                polygon_elev = max(polygon_elev, dem.array[row][col])

                polygon_depth = max(polygon_depth, sink_array[row][col])
                polygon_drain = max(polygon_drain, accumulation_array[row][col] * dem.geo_trans[1] * (-1*dem.geo_trans[5]))

            #Set Fill Volume, Fill Depth, Fill Elev and Drain Area fields
            polygon_feature.SetField("FillVolume", polygon_volume)
            polygon_feature.SetField("FillElev", float(polygon_elev))
            polygon_feature.SetField("FillDepth", float(polygon_depth))
            polygon_feature.SetField("DrainArea", int(polygon_drain))

            sink_poly_layer.SetFeature(polygon_feature)
            polygon_feature = None
            temp_polygon_layer = None
            temp_rasterize_ds = None

        else:
            sink_poly_layer.DeleteFeature(polygon_feature.GetFID())

    temp_rasterize_ds = None
    temp_polygon_ds = None
    sink_poly_ds = None
