from osgeo import ogr, gdal, osr
import numpy as np

#watershed_Boundary
############################################################################
#   For a generated Pourpoint shapefile (containing point/multi-point geometry),
#   create a MultiPolygon shapefile of sub watersheds within the DEM
#
###########################################################################

def watershed_Boundary(dem, watershed_filename,  pourpoint_shapefile, accumulation_array, direction_array):

    #Open Pourpoint shapefile
    drv = ogr.GetDriverByName("ESRI Shapefile")
    pourpoint_ds = drv.Open(pourpoint_shapefile, 0)
    pourpoint_layer = pourpoint_ds.GetLayer()

    pourpoint_list = []

    #Access Feature (theorectically once)
    for feature in pourpoint_layer:

        #Grab the MultiPoint Geometry
        geom = feature.GetGeometryRef()

        utm_point = (geom.GetX(), geom.GetY())
        matrix_point = dem.utm_to_coord(utm_point)
        pourpoint_value = accumulation_array[matrix_point[1]][matrix_point[0]]
        pourpoint_name = feature.GetField("Name")

        temp_pourpoint = (pourpoint_value, matrix_point[0], matrix_point[1], pourpoint_name)

        pourpoint_list.append(temp_pourpoint)

    #Sort list by lowest accumulation -> highest
    pourpoint_list.sort(key=lambda tup: tup[0])
    max_pourpoint = pourpoint_list[-1]

    catchment_array = np.full(dem.array_size, dem.nodata_value)
    catchment_stack = []
    curr_pixel = [max_pourpoint[2], max_pourpoint[1], 0]
    catchment_stack.append(curr_pixel)

    while len(catchment_stack) > 0:

        curr_pixel = catchment_stack[-1]

        if curr_pixel[2] < 8:
            row = curr_pixel[0] + dem.dY[curr_pixel[2]]
            col = curr_pixel[1] + dem.dX[curr_pixel[2]]

            if dem.within_domain(row, col) and direction_array[row][col] == dem.dDirection_reverse[curr_pixel[2]]:
                temp_pixel = [row, col, 0]
                catchment_stack.append(temp_pixel)

            curr_pixel[2] += 1
        else:
            catchment_array[curr_pixel[0]][curr_pixel[1]] = 1
            catchment_stack.pop()

    #Create output shapefile
    drv = ogr.GetDriverByName("ESRI Shapefile")
    watershed_ds = drv.CreateDataSource( watershed_filename )
    srs = osr.SpatialReference()
    srs.ImportFromWkt(dem.proj_wkt)
    watershed_layer = watershed_ds.CreateLayer(watershed_filename, srs)

    #Set subwatershed shapefile attributes
    name_field = ogr.FieldDefn("Name", ogr.OFTString)
    watershed_layer.CreateField(name_field)

    area_field = ogr.FieldDefn("Sq Miles", ogr.OFTReal)
    watershed_layer.CreateField(area_field)


    temp_catchment_ds = gdal.GetDriverByName('MEM').Create('', dem.array_size[1], dem.array_size[0], 1, gdal.GDT_Byte)
    temp_catchment_ds.SetProjection(srs.ExportToWkt())
    temp_catchment_ds.SetGeoTransform(dem.geo_trans)
    temp_catchment_ds.GetRasterBand(1).SetNoDataValue(float(dem.nodata_value))
    temp_catchment_ds.GetRasterBand(1).WriteArray(catchment_array)
    catchment_array = None

    gdal.Polygonize( temp_catchment_ds.GetRasterBand(1), temp_catchment_ds.GetRasterBand(1), watershed_layer, -1, [], callback=None )

    watershed_feature = watershed_layer.GetFeature(0)
    watershed_geom = watershed_feature.GetGeometryRef()

    #Calculate and set Sq Miles area field
    area = abs(watershed_geom.GetArea() * 0.00000038610)
    watershed_feature.SetField("Sq Miles", area)

    #Set the name of the subwatershed polygon to the same name as its pourpoint
    watershed_feature.SetField("Name", max_pourpoint[3])

    watershed_layer.SetFeature(watershed_feature)
    main_geom = None
    new_feature = None
