from osgeo import ogr, gdal, osr
import numpy as np
import sys

#Pourpoints
############################################################################
#   With a given set of Flow Direction and Flow Accumulation rasters, creates a
#   multi-point shapefile containing a number of subwatershed outlets
#
#   Assumes a subwatershed is equal in size to 1/10 of the domain
#
###########################################################################
def pourpoints(dem, accumulation_array, dir_array, pourpoint_filename):
    #Create pourpoint shapefile
    drv = ogr.GetDriverByName("ESRI Shapefile")
    pourpoint_ds = drv.CreateDataSource( pourpoint_filename )
    new_srs = osr.SpatialReference()
    new_srs.ImportFromWkt(dem.proj_wkt)
    pourpoint_layer = pourpoint_ds.CreateLayer(pourpoint_filename, new_srs)
    name_field = ogr.FieldDefn("Name", ogr.OFTString)
    pourpoint_layer.CreateField(name_field)


    seed_array = np.full(dem.array_size, -1, dtype=np.short)
    totalRuns = 0
    innerRuns = 0
    outerRuns = 0
    nrun = 0
    #Find the Border of Domain
    for y in range(dem.array_size[0]):
        for x in range(dem.array_size[1]):
            outerRuns+=1
            curr_elev = dem.array[y][x]
            if curr_elev != dem.nodata_value:
                innerRuns+=1
                for count in range(8):
                    nrun+=1
                    col = x + dem.dX[count]
                    row = y + dem.dY[count]

                    #Change 1
                    if (not (dem.within_domain(row, col)) or (dem.array[row][col] == dem.nodata_value)):
                        totalRuns+=1
                        seed_array[y][x] = 1
                        break

    #Using the same logic as the FlowLines function, determine if a border pixel is a pourpoint depending if the accumulation value
    # is above a threshold (ie, 0.5 km2)
    lower_limit = 20234 / (-1 * dem.geo_trans[5] * dem.geo_trans[1])


    initial_pourpoints = []

    #print("total runs {}".format(totalRuns))
    #print("Inner runs: {}".format(innerRuns))
    #print("Outer runs: {}".format(outerRuns))
    #print("Adjacent runs: {}".format(nrun))

    rows, cols = np.where(seed_array == 1)
    seed_array = None
    for i in range(len(rows)):
        curr_row = rows[i]
        curr_col = cols[i]

        accumulation_value = accumulation_array[curr_row][curr_col]
        if accumulation_value >= lower_limit:
            temp_location = (accumulation_value, curr_row, curr_col)
            initial_pourpoints.append(temp_location)
            #print("raster vals: {}".format(accumulation_array[curr_row][curr_col]))


    #print("Pour points size: {}".format(len(initial_pourpoints)))
    #print("geotransform vals: ")
    #print(dem.geo_trans)
    #print("lower limit: {}".format(lower_limit));
    #Follow upstream direction to find subwaterheds of significant size
    #Pourpoints should be spearated in Flow Accumulation by at least 1/10 of total domain size

    domain_size = dem.array_size[0] * dem.array_size[1]
    watershed_size = int(domain_size / 10)
    pourpoint_list = []


    for pourpoint in initial_pourpoints:
        upstream_stack = []

        #Each item in stack holds a Flow accumulation threshold, Pixel's row, Pixel's column, and direction to search for new pixels
        curr_pixel = [pourpoint[0] - watershed_size, pourpoint[1], pourpoint[2], 0]
        upstream_stack.append(curr_pixel)

        print("Pour point list length: {}".format(len(pourpoint_list)))
        while len(upstream_stack) > 0:

            curr_pixel = upstream_stack[-1]
            accum_threshold = curr_pixel[0]

            #Detemines if the current pixel is a valid pourpoint based on flow accumulation and existing threshold
            curr_accum = accumulation_array[curr_pixel[1]][curr_pixel[2]]

            if curr_accum <= accum_threshold and curr_accum > lower_limit:
                accum_threshold = curr_accum - watershed_size
                pourpoint_list.append((curr_pixel[1], curr_pixel[2]))

            if curr_pixel[3] < 8:
                row = curr_pixel[1] + dem.dY[curr_pixel[3]]
                col = curr_pixel[2] + dem.dX[curr_pixel[3]]
                #Add a new pixel that's further upstream

                if dem.within_domain(row, col) and dir_array[row][col] == dem.dDirection_reverse[curr_pixel[3]]:
                    temp_pixel = [accum_threshold, row, col, 0]
                    #print("Adding...")
                    upstream_stack.append(temp_pixel)
                curr_pixel[3] += 1
            else:
                #print("Removing...")
                upstream_stack.pop()

        #Add the initial pourpoint into the main pourpoint list
        temp_pourpoint = (pourpoint[1], pourpoint[2])
        pourpoint_list.append(temp_pourpoint)

    print("Pour point list length FINAL: {}".format(len(pourpoint_list)))

    for pourpoint in pourpoint_list:

        pourpoint_featureDefn = pourpoint_layer.GetLayerDefn()
        new_feature = ogr.Feature(pourpoint_featureDefn)
        pourpoint_geom = ogr.Geometry(ogr.wkbPoint)
        utm_coord = dem.coord_to_utm(pourpoint)
        pourpoint_geom.AddPoint(utm_coord[0], utm_coord[1])

        new_feature.SetGeometry(pourpoint_geom)
        pourpoint_layer.CreateFeature(new_feature)
        pourpoint_ds = None
