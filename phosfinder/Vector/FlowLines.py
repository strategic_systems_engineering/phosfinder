from osgeo import ogr, gdal, osr
import numpy as np

#FlowLines
############################################################################
#   With a given set of Flow Direction and Flow Accumulation rasters, create
#   a line shapefile that overlays significant flow paths
#
###########################################################################
def flowlines(dem, accumulation_array, direction_array, flowline_filename):

    #Create new empty shapefile
    drv = ogr.GetDriverByName("ESRI Shapefile")
    flowline_ds = drv.CreateDataSource( flowline_filename )
    new_srs = osr.SpatialReference()
    new_srs.ImportFromEPSG(26914)
    flowline_layer = flowline_ds.CreateLayer(flowline_filename, srs = new_srs)

    type_field = ogr.FieldDefn("Type", ogr.OFTString)
    type_field.SetWidth(24)
    flowline_layer.CreateField(type_field)

    acc_field = ogr.FieldDefn("Flow Acc", ogr.OFTInteger)
    flowline_layer.CreateField(acc_field)

    #Offset from upper-left corner to centroid of the pixel
    offset_x = dem.geo_trans[1] / 2
    offset_y = dem.geo_trans[5] / 2

    lower_limit = 20234 / (-1 * dem.geo_trans[5] * dem.geo_trans[1])
    upper_limit = 500000 / (-1 * dem.geo_trans[5] * dem.geo_trans[1])


    for row in range(dem.array_size[0]):
        for col in range(dem.array_size[1]):

            if accumulation_array[row][col] < lower_limit or direction_array[row][col] <= 0:
                continue

            else:
                feature = ogr.Feature(flowline_layer.GetLayerDefn())
                multiline = ogr.Geometry(ogr.wkbMultiLineString)


                #Create Line between current pixel and the neighbour pixel
                #Recall: having a direction over 0  means flow continues downstream
                curr_point_X = dem.geo_trans[0] + col * dem.geo_trans[1] + offset_x
                curr_point_Y = dem.geo_trans[3] + row * dem.geo_trans[5] + offset_y

                direction = direction_array[row][col]
                dir_index = dem.dDirection.index(direction)
                neighbour_col = col + dem.dX[dir_index]
                neighbour_row = row + dem.dY[dir_index]

                neighbour_point_X = dem.geo_trans[0] + neighbour_col * dem.geo_trans[1] + offset_x
                neighbour_point_Y = dem.geo_trans[3] + neighbour_row * dem.geo_trans[5] + offset_y

                line = ogr.Geometry(ogr.wkbLineString)
                line.AddPoint(curr_point_X, curr_point_Y)
                line.AddPoint(neighbour_point_X, neighbour_point_Y)
                multiline.AddGeometry(line)

                feature.SetGeometry(multiline)

                if accumulation_array[row][col] > upper_limit:
                    feature.SetField("Type", 'In-Channel')
                else:
                    feature.SetField("Type", 'Overland')

                feature.SetField("Flow Acc", int(accumulation_array[row][col]))

                flowline_layer.CreateFeature(feature)
                feature = None


    flowline_ds = None
