from osgeo import ogr, gdal, osr
import numpy as np

#subwatersheds
############################################################################
#   For a generated Pourpoint shapefile (containing point/multi-point geometry),
#   create a MultiPolygon shapefile of sub watersheds within the DEM
#
###########################################################################

def subwatersheds(dem, subwatersheds_filename, pourpoint_shapefile, accumulation_array, direction_array):

    #Create Subwatershed shapefile
    drv = ogr.GetDriverByName("ESRI Shapefile")
    subwatershed_ds = drv.CreateDataSource( subwatersheds_filename )
    new_srs = osr.SpatialReference()
    new_srs.ImportFromWkt(dem.proj_wkt)
    subwatershed_layer = subwatershed_ds.CreateLayer(subwatersheds_filename, new_srs,  ogr.wkbPolygon )

    #Set subwatershed shapefile attributes
    name_field = ogr.FieldDefn("Name", ogr.OFTString)
    subwatershed_layer.CreateField(name_field)

    area_field = ogr.FieldDefn("Sq Miles", ogr.OFTReal)
    subwatershed_layer.CreateField(area_field)

    #Open Pourpoint shapefile
    pourpoint_ds = drv.Open(pourpoint_shapefile, 0)
    pourpoint_layer = pourpoint_ds.GetLayer()

    pourpoint_list = []

    #Access Geospatial data in the pourpoint shapefile
    for feature in pourpoint_layer:

        geom = feature.GetGeometryRef()



        utm_point = (geom.GetX(), geom.GetY())
        matrix_point = dem.utm_to_coord(utm_point)
        pourpoint_value = accumulation_array[matrix_point[1]][matrix_point[0]]
        pourpoint_name = feature.GetField("Name")

        temp_pourpoint = (pourpoint_value, matrix_point[0], matrix_point[1], pourpoint_name)

        pourpoint_list.append(temp_pourpoint)


    subwatershed_array = np.full(dem.array_size, -1)

    #Sort list by lowest accumulation -> highest
    pourpoint_list.sort(key=lambda tup: tup[0])
    num_pourpoints = len(pourpoint_list)

    #Determine the catchment areas of the pourpoints
    #Should not double count pixels as we move downstream
    for i in range(num_pourpoints):
        pourpoint = pourpoint_list[i]

        catchment_stack = []
        curr_pixel = [pourpoint[2], pourpoint[1], 0]
        catchment_stack.append(curr_pixel)

        while len(catchment_stack) > 0:

            curr_pixel = catchment_stack[-1]

            if curr_pixel[2] < 8:
                row = curr_pixel[0] + dem.dY[curr_pixel[2]]
                col = curr_pixel[1] + dem.dX[curr_pixel[2]]

                if dem.within_domain(row, col) and subwatershed_array[row][col] < 0 and direction_array[row][col] == dem.dDirection_reverse[curr_pixel[2]]:
                    temp_pixel = [row, col, 0]
                    catchment_stack.append(temp_pixel)

                curr_pixel[2] += 1
            else:
                subwatershed_array[curr_pixel[0]][curr_pixel[1]] = i
                catchment_stack.pop()

    #Store subwatershed array in a Raster Band (temporary in-memory raster file)
    temp_subwatershed_ds = gdal.GetDriverByName('MEM').Create('', dem.array_size[1], dem.array_size[0], 1, gdal.GDT_Byte)
    temp_subwatershed_ds.SetProjection(new_srs.ExportToWkt())
    temp_subwatershed_ds.SetGeoTransform(dem.geo_trans)
    temp_subwatershed_ds.GetRasterBand(1).SetNoDataValue(float(dem.nodata_value))
    temp_subwatershed_ds.GetRasterBand(1).WriteArray(subwatershed_array)

    #Store subwatershed_mask in a Raster Band (temporary in-memory raster file)
    temp_mask_ds = gdal.GetDriverByName('MEM').Create('', dem.array_size[1], dem.array_size[0], 1, gdal.GDT_Byte)
    temp_mask_ds.SetProjection(new_srs.ExportToWkt())
    temp_mask_ds.SetGeoTransform(dem.geo_trans)
    temp_mask_ds.GetRasterBand(1).SetNoDataValue(float(dem.nodata_value))
    #temp_mask_ds.GetRasterBand(1).WriteArray(subwatershed_mask_array)

    temp_polygon_ds = ogr.GetDriverByName('MEMORY').CreateDataSource( 'memData' )
    #temp_polygon_layer = temp_polygon_ds.CreateLayer('', new_srs, ogr.wkbPolygon)


    #Creates Polygons from each designated subwatershed found in the subwatershed array
    for subwatershed_id in range(num_pourpoints):

        temp_polygon_layer = temp_polygon_ds.CreateLayer('', new_srs, ogr.wkbPolygon)

        #Find all pixels of a certain subwatershed_id
        subwatershed_mask_array = subwatershed_array == subwatershed_id
        subwatershed_mask_array.astype(np.int)

        #Create an appropriate boolean mask for the subwatershed_id
        temp_mask_ds.GetRasterBand(1).WriteArray(subwatershed_mask_array)

        #Create Polygon from the subwatershed_array
        gdal.Polygonize( temp_subwatershed_ds.GetRasterBand(1), temp_mask_ds.GetRasterBand(1), temp_polygon_layer, -1, [], callback=None )

        #Polygonize may create more than one Polygon
        #Therefore, we must ensure that these multiple polygons are considered as a combined polygon
        main_geom = ogr.Geometry(ogr.wkbPolygon)
        for i in range(temp_polygon_layer.GetFeatureCount()):

            temp_feature = temp_polygon_layer.GetFeature(i)
            temp_geom = temp_feature.GetGeometryRef()

            #main_geom = main_geom.Union(temp_geom)
            main_geom.AddGeometry(temp_geom.GetGeometryRef(0))

        temp_polygon_layer = None


        #Create a new Polygon Feature into the output shapefile
        subwatershed_featureDefn = subwatershed_layer.GetLayerDefn()
        new_feature = ogr.Feature(subwatershed_featureDefn)
        new_feature.SetGeometry(main_geom)

        #Calculate and set Sq Miles area field
        area = abs(main_geom.GetArea() * 0.00000038610)
        new_feature.SetField("Sq Miles", area)

        #Set the name of the subwatershed polygon to the same name as its pourpoint
        new_feature.SetField("Name", pourpoint_list[subwatershed_id][3])

        subwatershed_layer.CreateFeature(new_feature)
        main_geom = None
        new_feature = None

    temp_subwatershed_ds = None
    temp_mask_ds = None
    temp_polygon_ds = None

    #Give each subwatershed polygon att
    subwatershed_ds = None
