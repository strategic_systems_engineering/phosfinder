import math
from osgeo import gdal, osr, ogr
import numpy as np
import bisect
import heapq

from .raster import Raster

class Dem(Raster):

    dX = [1, 1, 0, -1, -1, -1, 0, 1]
    dY = [0, 1, 1, 1, 0 ,-1, -1, -1]
    dDirection= [1, 2, 4, 8, 16, 32, 64, 128]
    dDirection_reverse = [16, 32, 64, 128, 1, 2, 4, 8]

    def __init__(self, dem_filename):
        Raster.__init__(self, dem_filename)
        cell_size_X = self.geo_trans[1]
        cell_size_Y = -1*self.geo_trans[5]
        cell_size_diagonal = math.sqrt((cell_size_X**2 +  cell_size_Y**2))
        self.dLength = [cell_size_diagonal, cell_size_X, cell_size_diagonal, cell_size_Y, cell_size_diagonal, cell_size_X, cell_size_diagonal, cell_size_Y]


    #Find Catchment
    ###########################################################################
    #   For a given outlet location, find the upstream pixels where water
    #   will flow into the outlet
    #
    ###########################################################################
    def find_catchment(self, outlet_point, direction_array, catchment_array):

        catchment_array[outlet_point[0]][outlet_point[1]] = 1
        for count in range(8):
            row = outlet_point[0] + self.dY[count]
            col = outlet_point[1] + self.dX[count]

            if self.within_domain(row, col) and direction_array[row][col] == self.dDirection_reverse[count]:
                temp_point = (row, col)
                self.find_catchment(temp_point, direction_array, catchment_array)


    #convex
    ###########################################################################
    #   Analyzes a raster, before it gets polygonized, to ensure it does not create
    #       duplicate vertices that crate convex polygons. This prevents the polygon
    #       from being invalid
    #
    ###########################################################################

    def check_convex(raster_array):
        temp_array = raster_array
        tolerance = 2

        for j in range(self.array_size[0]):

            if j > 0 and j < self.array_size[0]-1:
              for i in range(self.array_size[1]):

                if i > 0 and i < self.array_size[1]-1:
                    # evaulate the centre, dump results from reading 1st array into 2nd array
                    if raster_array[j, i] > 0 and np.sum(raster_array[j-1 : j+2 , i-1 : i+2]) <= tolerance:
                        temp_array[j] = 0

                else:
                    # left and rightmost column to also be 0's
                    temp_array[j, i] = 0

            ## Top and bottom row to be 0's
            else:
                temp_array[j] = 0

            if j == 1:
              print("")
              print("Convex Cleanup >>> ")
              print("0% done")
            if j == (self.array_size[0] // 4):
              print("25% done")
            if j == (self.array_size[0] // 2):
              print("50% done")
            if j == ((self.array_size[0] // 2) + (self.array_size[0]// 4)):
              print("75% done")
            if j == (self.array_size[0] - 1):
              print("100% done")
              print("")

        return temp_array


    #concave
    ###########################################################################
    #   Analyzes a raster, before it gets polygonized, to ensure it does not create
    #       duplicate vertices that creates convex polygons. This prevents the
    #       polygon from being invalid
    #
    ###########################################################################

    def check_concave(raster_array):
        temp_array = raster_array
        tolerance = 7

        for j in range(self.array_size[0]):

            if j > 1 and j < self.array_size[0]-1:
                for i in range(self.array_size[1]):
                    if i <= 1 or i >= self.array_size[1]-1:

                        # left and rightmost column to also be 0's
                        temp_array[j, i] = 0

                    elif i > 1 and i < self.array_size[1]-1:
                        # evaulate the centre, dump results from reading 2nd array into final array

                        if raster_array[j, i] < 1 and np.sum(raster_array[j-1 : j+2 , i-1 : i+2]) >= tolerance:
                            temp_array[j, i] = 1

            else:
                # top and bottom row to be 0's
                temp_array[j] = 0

            if j == 1:
                print("Concave Cleanup >>> ")
                print("0% done")
            if j == (self.array_size[0] // 4):
                print("25% done")
            if j == (self.array_size[0] // 2):
                print("50% done")
            if j == ((self.array_size[0] // 2) + (self.array_size[0] // 4)):
                print("75% done")
            if j == (self.array_size[0] - 1):
                print("100% done")
                print("")

        return temp_array


    #Fill Sinks
    ###########################################################################
    #   Using the Wang & Liu method, fill depressions in the DEM (replaccing
    #    the elevations within the DEM)
    #       * Will also produce a watershed dileneated array as a by-product
    #
    ###########################################################################
    def fill_sinks(self, min_slope):
        print('Filling sinks with minimun slope: {} degress'.format(min_slope))

        min_diff = []

        if min_slope > 0:
            #min_slope = math.tan(math.radians(min_slope))
            min_slope = math.tan(min_slope * (math.pi / 180.0))
            for count in range(8):
                min_diff.append( min_slope * self.dLength[count] )

            preserve = True
        else:
            preserve = False

        priority_queue = []
        filled_array = np.full(self.array_size, self.nodata_value, dtype=np.float32)
        seed_array = np.full(self.array_size, -1)
        id = 0

        #Seed (Border of Domain)
        for y in range(self.array_size[0]):
            for x in range(self.array_size[1]):

                curr_elev = self.array[y][x]

                if curr_elev != self.nodata_value:

                    for count in range(8):

                        col = x + self.dX[count]
                        row = y + self.dY[count]

                        #Change 1
                        if not (self.within_domain(row, col)) or self.array[row][col] == self.nodata_value:

                            filled_array[y][x] = curr_elev
                            seed_array[y][x] = 1
                            id += 1

                            temp_coord = (curr_elev, y, x)
                            #bisect.insort(priority_queue, temp_coord)
                            priority_queue.append(temp_coord)
                            break

        heapq.heapify(priority_queue)
        while len(priority_queue) > 0:
            curr_cell = heapq.heappop(priority_queue)
            curr_elev= filled_array[curr_cell[1]][curr_cell[2]]

            for count in range(8):

                neighbour_col = curr_cell[2] + self.dX[count]
                neighbour_row = curr_cell[1] + self.dY[count]

                if self.within_domain(neighbour_row, neighbour_col) and self.array[neighbour_row][neighbour_col] != self.nodata_value:
                    neighbour_elev = self.array[neighbour_row][neighbour_col]

                    if filled_array[neighbour_row][neighbour_col] == self.nodata_value:
                        if preserve:
                            if neighbour_elev < (curr_elev + min_diff[count]):
                                neighbour_elev = curr_elev + min_diff[count]

                        elif neighbour_elev <= curr_elev:
                            neighbour_elev = curr_elev

                        temp_coord = (neighbour_elev, neighbour_row, neighbour_col)
                        heapq.heappush(priority_queue,temp_coord)
                        filled_array[neighbour_row][neighbour_col] = neighbour_elev

        seed_array = None
        self.array = filled_array

    def fill_sinks_old(self, min_slope):
        print('Filling sinks with minimun slope: {} degress'.format(min_slope))

        min_diff = []

        if min_slope > 0:
            #min_slope = math.tan(math.radians(min_slope))
            min_slope = math.tan(min_slope * (math.pi / 180.0))
            for count in range(8):
                min_diff.append( min_slope * self.dLength[count] )

            preserve = True
        else:
            preserve = False

        priority_queue = []
        filled_array = np.full(self.array_size, self.nodata_value, dtype=np.float32)
        seed_array = np.full(self.array_size, -1)
        id = 0

        #Seed (Border of Domain)
        for y in range(self.array_size[0]):
            for x in range(self.array_size[1]):

                curr_elev = self.array[y][x]

                if curr_elev != self.nodata_value:

                    for count in range(8):

                        col = x + self.dX[count]
                        row = y + self.dY[count]

                        #Change 1
                        if not (self.within_domain(row, col)) or self.array[row][col] == self.nodata_value:

                            filled_array[y][x] = curr_elev
                            seed_array[y][x] = 1
                            id += 1

                            temp_coord = (curr_elev, y, x)
                            bisect.insort(priority_queue, temp_coord)
                            #priority_queue.append(temp_coord)
                            break



        while len(priority_queue) > 0:
            curr_cell = priority_queue.pop(0)
            curr_elev= filled_array[curr_cell[1]][curr_cell[2]]

            for count in range(8):

                neighbour_col = curr_cell[2] + self.dX[count]
                neighbour_row = curr_cell[1] + self.dY[count]

                if self.within_domain(neighbour_row, neighbour_col) and self.array[neighbour_row][neighbour_col] != self.nodata_value:
                    neighbour_elev = self.array[neighbour_row][neighbour_col]

                    if filled_array[neighbour_row][neighbour_col] == self.nodata_value:
                        if preserve:
                            if neighbour_elev < (curr_elev + min_diff[count]):
                                neighbour_elev = curr_elev + min_diff[count]

                        elif neighbour_elev <= curr_elev:
                            neighbour_elev = curr_elev

                        temp_coord = (neighbour_elev, neighbour_row, neighbour_col)
                        bisect.insort(priority_queue, temp_coord)

                        filled_array[neighbour_row][neighbour_col] = neighbour_elev

        seed_array = None
        self.array = filled_array


    #Flow Direction
    ############################################################################
    #   Determine each cell's Flow Direction based on the neighbouring pixel with steepest descent in elevation
    #   the steepest descent in elevation
    #       * Direction represented as 2^X, where X is a number 1 - 8
    #       * Clockwise starting at NE -> N
    #
    ###########################################################################
    def flow_direction(self):
        print("Drainage Direction Assignment (D8)")
        direction_array = np.full(self.array_size, -1, dtype=np.int32)

        for y in range(self.array_size[0]):
            for x in range(self.array_size[1]):

                max_slope = 0
                direction = 0
                curr_cell = self.array[y][x]

                if curr_cell != self.nodata_value:

                    for count in range(8):
                        col = x + self.dX[count]
                        row = y + self.dY[count]

                        if self.within_domain(row, col):

                            neighbour_cell = self.array[row][col]

                            if neighbour_cell != self.nodata_value:

                                slope = (curr_cell - neighbour_cell) / self.dLength[count]

                                if slope > max_slope and slope > 0:
                                    max_slope =  slope
                                    direction = self.dDirection[count]
                                else:
                                    continue
                            else:
                                continue
                        else:
                            continue

                    if max_slope == 0:
                        direction_array[y][x] = 0
                    else:
                        direction_array[y][x] = direction

        return direction_array




    #Drainage Feature Assignment
    ############################################################################
    #   Based on Flow Directions, stores ecah cells' relationship with its neighbours
    #   Relationship is represented in a single 3-digit value
    #       * Number of neightbours flowing into cell (input) in hundreds place
    #       * Numner of neighbours (0 or 1) flowing out towards in ones place
    #
    ###########################################################################
    def drainage_features(self, direction_array):
        print("Drainage Feature")
        feature_array = np.full(self.array_size, -1)
        for y in range(self.array_size[0]):
            for x in range(self.array_size[1]):

                if direction_array[y][x] > 0:
                    num_cell_output = 1
                else:
                    num_cell_output = 0


                num_cell_input = 0
                for count in range(8):
                    col = x + self.dX[count]
                    row = y + self.dY[count]

                    if self.within_domain(row, col):

                        neighbour_cell = direction_array[row][col]
                        if neighbour_cell == self.dDirection_reverse[count]:
                            num_cell_input += 1
                        else:
                            continue
                    else:
                        continue

                feature_array[y][x] = (num_cell_input * 100) + num_cell_output

        return feature_array




    #Flow Accumulation
    ############################################################################
    #   At each pixel, determine the number of upstream pixels flow into
    #   the current pixel.
    #
    ###########################################################################
    def flow_accumulation(self, direction_array):
        print("Flow Accumulation")

        feature_array = self.drainage_features(direction_array)

        accumulation_array = np.full(self.array_size, 1)
        temp_array = feature_array // 100

        stack = []
        for y in range(self.array_size[0]):
            for x in range(self.array_size[1]):

                if temp_array[y][x] == 0 and feature_array[y][x] != 0:
                    stack.append((y, x))

        count = 0
        while len(stack) > 0:
            count += 1
            #curr_cell = stack.pop(0)
            curr_cell = stack.pop()
            row = curr_cell[0]
            col = curr_cell[1]

            curr_accum = accumulation_array[row][col]

            temp_array[row][col] -= 1

            direction = direction_array[row][col]
            if direction > 0:

                index = self.dDirection.index(direction)

                neighbour_row = row + self.dY[index]
                neighbour_col = col + self.dX[index]

                if self.within_domain(neighbour_row, neighbour_col):
                    #accumulation_array[neighbour_row][neighbour_col] += curr_accum + 1
                    accumulation_array[neighbour_row][neighbour_col] += curr_accum
                    temp_array[neighbour_row][neighbour_col] -= 1

                    if temp_array[neighbour_row][neighbour_col] == 0:
                        stack.append((neighbour_row, neighbour_col))

        return accumulation_array


    #Catchment Dileneation
    ############################################################################
    #   Highlight which upstream pixels flow into a particular outlet cell (UTM)
    #
    ###########################################################################
    def dileneate_catchment(self, direction_array, utm_outlet):
        print("Finding Catchment at {}".format(utm_outlet))
        catchment_array = np.full(self.array_size, self.nodata_value)

        matrix_point = self.utm_to_coord(utm_outlet)

        catchment_array[matrix_point[1]][matrix_point[0]] = 1

        for count in range(8):
            row = matrix_point[0] + self.dY[count]
            col = matrix_point[1] + self.dX[count]

            if self.within_domain(row, col) and direction_array[row][col] == self.dDirection_reverse[count]:
                temp_point = (row, col)
                self.find_catchment(temp_point, direction_array, catchment_array)
        return catchment_array

    #Catchment Dileneation (Iterative)
    ############################################################################
    #   Highlight which upstream pixels flow into a particular outlet cell (UTM)
    #
    #
    ###########################################################################
    def dileneate_catchment_v2(self, direction_array, utm_outlet):
        print("Finding Catchment at {}".format(utm_outlet))
        catchment_array = np.full(self.array_size, self.nodata_value)

        matrix_point = self.utm_to_coord(utm_outlet)

        #catchment_array[matrix_pointY][matrix_pointX] = 1
        catchment_stack = []
        curr_pixel = [matrix_point[1], matrix_point[0], 0]
        catchment_stack.append(curr_pixel)


        while len(catchment_stack) > 0:

            curr_pixel = catchment_stack[-1]

            if curr_pixel[2] < 8:

                row = curr_pixel[0] + self.dY[curr_pixel[2]]
                col = curr_pixel[1] + self.dX[curr_pixel[2]]

                if self.within_domain(row, col) and direction_array[row][col] == self.dDirection_reverse[curr_pixel[2]]:

                    temp_pixel = [row, col, 0]
                    catchment_stack.append(temp_pixel)

                curr_pixel[2] += 1
            else:

                catchment_array[curr_pixel[0]][curr_pixel[1]] = 1
                catchment_stack.pop()
                #del(curr_pixel)

        return catchment_array



    #Raster output
    ############################################################################
    #   Create a raster output for any of the arrays
    #
    ###########################################################################
    def output_raster(self, array, filename, **kwargs):

        if 'nodata_override' in kwargs:
            nodata_value = float(kwargs.get('nodata_override'))
        else:
            nodata_value = float(self.nodata_value)

        driver = gdal.GetDriverByName('GTiff')
        new_raster_ds = driver.Create(filename, array.shape[1], array.shape[0], 1, gdal.GDT_Float32)
        srs = osr.SpatialReference()
        srs.ImportFromEPSG(26914)
        new_raster_ds.SetProjection(srs.ExportToWkt())
        new_raster_ds.SetGeoTransform(self.geo_trans)
        new_raster_ds.GetRasterBand(1).SetNoDataValue(nodata_value)
        new_raster_ds.GetRasterBand(1).WriteArray(array)
        new_raster_ds.FlushCache()


    #Outlet Catchment
    ############################################################################
    #   For a generated Catchment Raster file, create a Polygon shapefile of the
    #   catchment
    #
    ###########################################################################
    @staticmethod
    def output_catchment_shp(catchment_raster, filename):
        catchment_ds = gdal.Open(catchment_raster)

        dst_layername = filename
        drv = ogr.GetDriverByName("ESRI Shapefile")
        dst_ds = drv.CreateDataSource( dst_layername )

        dst_layer = dst_ds.CreateLayer(dst_layername, srs = None )
        gdal.Polygonize( catchment_ds.GetRasterBand(1), catchment_ds.GetRasterBand(1), dst_layer, -1, [], callback=None )

        dst_ds = None


    #Slope_raster
    ############################################################################
    # Calculates the slope of a central cell in the middle of a 3x3 matrix
    #`Slope is given as a percent slope expressed as a decimal number
    ###########################################################################

    def slope_raster(self, direction_array):
        slope_raster = np.full(self.array_size, self.nodata_value)

        for y in range(self.array_size[0]):
            for x in range(self.array_size[1]):

                curr_cell = self.array[y][x]

                if curr_cell != self.nodata_value:

                    curr_direction = direction_array[y][x]
                    if curr_direction > 0:
                        index = self.dDirection.index(curr_direction)

                        length = self.dLength[index]
                        row = y + self.dY[index]
                        col = x + self.dX[index]

                        if self.within_domain(row, col):

                            neighbour_cell = dem.dem_array[row][col]
                            slope = (curr_cell - neighbour_cell) / length
                            slope_raster[y][x] = slope
        return slope_raster


    #Slope_raster
    ############################################################################
    # Calculates the slope of a central cell in the middle of a 3x3 matrix
    #`Slope is given as a percent slope expressed as a decimal number
    ###########################################################################

    def slope_raster(self):
        print('Creating Slope Array')
        slope_raster = np.full(self.array_size, self.nodata_value)

        for y in range(self.array_size[0]):
            for x in range(self.array_size[1]):

                max_slope = 0
                direction = 0
                curr_cell = self.array[y][x]

                if curr_cell != self.nodata_value:

                    for count in range(8):
                        col = x + self.dX[count]
                        row = y + self.dY[count]

                        if self.within_domain(row, col):

                            neighbour_cell = self.array[row][col]

                            if neighbour_cell != self.nodata_value:

                                slope = (curr_cell - neighbour_cell) / self.dLength[count]

                                if slope > max_slope and slope > 0:
                                    max_slope =  slope
                                    direction = self.dDirection[count]
                                else:
                                    continue
                            else:
                                continue
                        else:
                            continue


                    slope_raster[y][x] = max_slope

        return slope_raster
