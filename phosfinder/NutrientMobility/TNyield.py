import numpy as np

#tn_yield
############################################################################
# Determines annual nitrogen yield leaving the landsacpe based on land cover
#  usage.
# Metric is in kg/ha/year
#
#   [Should this be within another object?]
############################################################################
def tn_yield(dem, crop_raster):
    print("Determining yearly Total Nitrogen loss (in grams) per pixel")
    crop_geo_trans = crop_raster.geo_trans
    tn_yield_array = np.full(dem.array_size, dem.nodata_value)
    cell_area = dem.geo_trans[1] * (-1*dem.geo_trans[5])

    for y in range(dem.array_size[0]):
        for x in range(dem.array_size[1]):
            if dem.array[y][x] != dem.nodata_value:

                utm_coord = dem.coord_to_utm([y, x])
                crop_x = int((utm_coord[0] - crop_geo_trans[0]) / crop_geo_trans[1])
                crop_y = int((utm_coord[1] - crop_geo_trans[3]) / crop_geo_trans[5])

                crop_class = crop_raster.array[crop_y][crop_x]
                yield_value = 0

                if crop_class >= 200 or crop_class == 50:
                    yield_value = 0.2 * cell_area
                elif crop_class == 193 or crop_class == 110:
                    yield_value = 0.13 * cell_area
                elif crop_class >= 131 or crop_class == 120:
                    yield_value = 0.78 * cell_area
                elif crop_class == 122:
                    yield_value = 0.24 * cell_area
                elif crop_class == 35:
                    yield_value = 0.96 * cell_area
                elif crop_class == 34:
                    yield_value = 1.8 * cell_area
                else:
                    yield_value = 0.35 * cell_area

                tn_yield_array[y][x] = yield_value
                
    return tn_yield_array
