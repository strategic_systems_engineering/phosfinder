import numpy as np

#tp_yield
############################################################################
# Determines annual phosperus yield leaving the landsacpe based on land cover
#  usage.
# Metric is in kg/ha/year
#
#   [Should this be within another object?]
############################################################################
def tp_yield(dem, crop_raster):
    print("Determining yearly Total Phosperus loss (in grams) per pixel")
    crop_geo_trans = crop_raster.geo_trans
    tp_yield_array = np.full(dem.array_size, dem.nodata_value)
    cell_area = dem.geo_trans[1] * (-1*dem.geo_trans[5])

    for y in range(dem.array_size[0]):
        for x in range(dem.array_size[1]):
            if dem.array[y][x] != dem.nodata_value:

                utm_coord = dem.coord_to_utm([y, x])
                crop_x = int((utm_coord[0] - crop_geo_trans[0]) / crop_geo_trans[1])
                crop_y = int((utm_coord[1] - crop_geo_trans[3]) / crop_geo_trans[5])

                crop_class = crop_raster.array[crop_y][crop_x]
                yield_value = 0

                if crop_class >= 200 or crop_class == 50:
                    yield_value = 0.0075 * cell_area
                elif crop_class >= 131 or crop_class == 120:
                    yield_value = 0.038 * cell_area
                elif crop_class == 120 or crop_class == 110:
                    yield_value = 0.017 * cell_area
                elif crop_class >= 80 or crop_class <= 20:
                    yield_value = 0
                elif crop_class == 34 or crop_class == 35:
                    yield_value = 0.115 * cell_area
                elif crop_class == 30:
                    yield_value = 0.1 * cell_area

                tp_yield_array[y][x] = yield_value

    return tp_yield_array
