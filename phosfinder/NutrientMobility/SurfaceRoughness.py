import numpy as np


#surface_roughness
############################################################################
# Determines Manning's Roughness Coefficient based on land cover usage
#   [Should this be within another object?]
############################################################################
def surface_roughness(dem, crop_raster):
    print("Determining Manning's Roughness Coefficient")
    crop_geo_trans = crop_raster.geo_trans
    roughness_array = np.full(dem.array_size, dem.nodata_value)

    for y in range(dem.array_size[0]):
        for x in range(dem.array_size[1]):

            if dem.array[y][x] != dem.nodata_value:

                utm_coord = dem.coord_to_utm([y, x])

                crop_x = int((utm_coord[0] - crop_geo_trans[0]) / crop_geo_trans[1])
                crop_y = int((utm_coord[1] - crop_geo_trans[3]) / crop_geo_trans[5])

                crop_class = crop_raster.array[crop_y][crop_x]
                roughness_value = 0

                if crop_class >= 200:
                    roughness_value = 0.16
                elif crop_class >= 131 or crop_class == 110 or crop_class == 120:
                    roughness_value = 0.035
                elif crop_class == 122:
                    roughness_value = 0.03
                elif crop_class >= 80 or crop_class == 20:
                    roughness_value = 0.04
                elif crop_class == 34:
                    roughness_value = 0.08
                elif crop_class == 30:
                    roughness_value = 0.025
                elif crop_class <= 35:
                    roughness_value = 0.08
                else:
                    roughness_value = 0.1

                roughness_array[y][x] = roughness_value

    return roughness_array
