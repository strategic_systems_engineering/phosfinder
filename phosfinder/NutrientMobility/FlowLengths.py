import numpy as np


#Upstream Flow Length
############################################################################
# Determines the longest upstream flow length for each pixel of a sub-basin
#   identified by a given outlet
#
############################################################################

def upstream_length(dem, direction_array, utm_outlet):
    print("Determining the Upstream Flow Lengths of each pixel within the subbbasin of outlet {}, {}".format(utm_outlet[0], utm_outlet[1]))
    flow_lengths_array = np.full(dem.array_size, dem.nodata_value)

    matrix_point = dem.utm_to_coord(utm_outlet)

    #[y-coord, x-coord, direction, curr_max_distance]
    catchment_stack = []
    curr_pixel = [matrix_point[1], matrix_point[0], 0, 0]
    catchment_stack.append(curr_pixel)

    while len(catchment_stack) > 0:
        curr_pixel = catchment_stack[-1]

        if(curr_pixel[2] < 8):

            row = curr_pixel[0] + dem.dY[curr_pixel[2]]
            col = curr_pixel[1] + dem.dX[curr_pixel[2]]
            upstream_direction = dem.dDirection_reverse[curr_pixel[2]]

            if dem.within_domain(row, col) and direction_array[row][col] == upstream_direction:

                neightbour_flow_length = flow_lengths_array[row][col]
                curr_max_distance = curr_pixel[3]

                #Neighbouring pixels have not been processed yet. Process them first and go back to curr cell later
                if neightbour_flow_length == dem.nodata_value:
                    temp_pixel = [row, col, 0, 0]
                    catchment_stack.append(temp_pixel)

                #Neighbouring pixels have been processed.
                #Preform calculations and proceed
                else:
                    index = dem.dDirection.index(upstream_direction)
                    cell_length = dem.dLength[index]
                    curr_max_distance = max(curr_max_distance, neightbour_flow_length + cell_length)
                    curr_pixel[3] = curr_max_distance
                    curr_pixel[2] += 1

            #No neighbouring pixels at current upstream direction
            else:
                curr_pixel[2] += 1

        else:
            flow_lengths_array[curr_pixel[0]][curr_pixel[1]] = curr_pixel[3]
            catchment_stack.pop()

    return flow_lengths_array
