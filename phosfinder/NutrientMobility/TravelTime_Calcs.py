import math
import numpy as np


#calc_nrcs
############################################################################
# Determines Travel Time across a single cell using the NRCS method
# Different formulas are used depending on weither it's sheet, shallow or
#   channeld flow
#
# Travel Time given in hours
############################################################################

def calc_nrcs(dem_geo_trans, roughness_value, accumulation_value, discharge, length, slope, flow_length):
    curr_travel_time = 0
    cell_size_X = dem_geo_trans[1]
    cell_size_Y = -1*dem_geo_trans[5]

    #Convert 0.5km2 to number of upstream pixels
    upper_limit = 500000 / (-1 * dem_geo_trans[5] * dem_geo_trans[1])

    #Channeled Flow
    ###################################
    if accumulation_value > upper_limit:
        curr_travel_time = length /(3600 * ( 1.49 / roughness_value) * math.pow(cell_size_X , 2/3) * math.pow(slope , 0.5))

    #Shallow Flow
    ###################################
    elif flow_length > 100:
        curr_travel_time = length / (3600 * 16.13 * math.pow(slope, 0.5))

    #Sheet Flow
    ###################################
    else:
        curr_travel_time = (0.007 * math.pow(roughness_value * flow_length, 0.8)) / (math.pow(discharge,0.5)* math.pow(slope, 0.4))

    return curr_travel_time
