import numpy as np
import math

#Nutrient loss percent
############################################################################
# Determines the percentage of the annual nuutrient loading (either phosphorus
#   or nitrogen) leaving the landscape is due to runoff from preciptation
#
############################################################################

def nutrient_percent(dem, traveltime_array, time_units, accumulation_array):
    print("Calculating Nutrient Transport")

    nutrient_precent_array = np.full(dem.array_size, dem.nodata_value)

    #Percent decay equation requires Travel Time in Days
    time_unit_list = ['seconds', 'minutes', 'hours', 'days']
    time_unit_denoms_list= [86400, 1440, 24, 1]

    assert(time_units in time_unit_list)
    index = time_unit_list.index(time_units)
    time_denom = time_unit_denoms_list[index]
    upper_limit = 500000 / (-1 * dem.geo_trans[5] * dem.geo_trans[1])
    for y in range(dem.array_size[0]):
        for x in range(dem.array_size[1]):

            if traveltime_array[y][x] != dem.nodata_value:

                #Ensures Travel Time is represented in days
                travel_time = traveltime_array[y][x] / time_denom
                #In-channel
                if accumulation_array[y][x] > upper_limit:
                    decay_rate = 0.4
                #Overland
                else:
                    decay_rate = 0.1
                #print(-1 * decay_rate* travel_time)
                loss_value = math.exp(-1 * decay_rate* travel_time)

                nutrient_precent_array[y][x] = loss_value

    return nutrient_precent_array

#Nutrient loss mass
############################################################################
# Determines the mass of nutrients (either phosphorus or nitrogen) lost annually
#   due to runoff
#
# This version calculates from scratch
#
# Output given in Kilograms
############################################################################

def nutrient_mass(dem, nutrient_yield_array, traveltime_array, time_units, accumulation_array):
    print("Calculating Nutrient Transport")
    nutrient_loss_array = np.full(dem.array_size, dem.nodata_value)
    #Percent decay equation requires Travel Time in Days
    time_unit_list = ['seconds', 'minutes', 'hours', 'days']
    time_unit_denoms_list= [86400, 1440, 24, 1]

    assert(time_units in time_unit_list)
    index = time_unit_list.index(time_units)
    time_denom = time_unit_denoms_list[index]
    upper_limit = 500000 / (-1 * dem.geo_trans[5] * dem.geo_trans[1])
    for y in range(dem.array_size[0]):
        for x in range(dem.array_size[1]):
            if traveltime_array[y][x] != dem.nodata_value:
                #Ensures Travel Time is represented in days
                travel_time = traveltime_array[y][x] / time_denom
                #In-channel
                if accumulation_array[y][x] > upper_limit:
                    decay_rate = 0.4
                #Overland
                else:
                    decay_rate = 0.1
                #print(-1 * decay_rate* travel_time)
                loss_value = math.exp(-1 * decay_rate* travel_time)

                nutrient_loss_array[y][x] = loss_value * nutrient_yield_array[y][x]

    return nutrient_loss_array

#Nutrient loss mass
############################################################################
# Determines the mass of nutrients (either phosphorus or nitrogen) lost annually
#   due to runoff
#
# This version calculates from if the nutrient percent loss is already generated
#
# Output given in Kilograms
############################################################################
def nutrient_mass_from_percent(dem, nutrient_mass_array, nutrient_percent_array):
    print("Calculating Nutrient Transport")
    nutrient_loss_array = np.full(dem.array_size, dem.nodata_value)
    for y in range(dem.array_size[0]):
        for x in range(dem.array_size[1]):
            if nutrient_mass_array[y][x] != dem.nodata_value:
                nutrient_yield = nutrient_mass_array[y][x]
                percent_loss = nutrient_percent_array[y][x]
                nutrient_loss_array[y][x] = percent_loss * nutrient_yield

    return nutrient_loss_array


#Nutrient Accumulation
############################################################################
# Calculates each cell's Accumulated Nutrient Mass where each cell routes
#   flow towards a common outlet
#
# Output given in Kilograms
#############################################################################
def nutrient_accumulation(dem, nutrient_loss_array, direction_array, utm_outlet):
    print("Calculating Accumulated Nutrient Mass ")
    mass_accum_array = np.full(dem.array_size, dem.nodata_value)
    matrix_point = dem.utm_to_coord(utm_outlet)
    print(matrix_point)

    #[y-coord, x-coord, direction, curr_max_traveltime]
    catchment_stack = []
    temp_mass = nutrient_loss_array[matrix_point[1]][matrix_point[0]]
    curr_pixel = [matrix_point[1], matrix_point[0], 0, temp_mass]
    catchment_stack.append(curr_pixel)

    while len(catchment_stack) > 0:
        curr_pixel = catchment_stack[-1]

        if(curr_pixel[2] < 8):

            row = curr_pixel[0] + dem.dY[curr_pixel[2]]
            col = curr_pixel[1] + dem.dX[curr_pixel[2]]
            upstream_direction = dem.dDirection_reverse[curr_pixel[2]]

            if dem.within_domain(row, col) and direction_array[row][col] == upstream_direction:

                neighbour_accum = mass_accum_array[row][col]
                #Neighbouring pixels have not been processed yet. Process them first and go back to current cell later
                if neighbour_accum == dem.nodata_value:
                    temp_mass = nutrient_loss_array[row][col]
                    temp_pixel = [row, col, 0, temp_mass]
                    catchment_stack.append(temp_pixel)

                #Neighbouring pixels have been processed.
                #Preform calculations and proceed
                else:
                    curr_pixel[3] += neighbour_accum
                    curr_pixel[2] += 1

            #No neighbouring pixels at current upstream direction
            else:
                curr_pixel[2] += 1

        else:
            mass_accum_array[curr_pixel[0]][curr_pixel[1]] = curr_pixel[3]
            catchment_stack.pop()

    return mass_accum_array
