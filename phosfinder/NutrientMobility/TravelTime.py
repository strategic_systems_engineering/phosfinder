import math
import numpy as np
from .TravelTime_Calcs import calc_nrcs


#Travel_time
############################################################################
# Determines Manning's Roughness Coefficient based on land cover usage
#   Requires a Discharge metric giveen in m3/s
#   [Should this be within another object?]
#
#   Determines time in Hours
############################################################################
def travel_time_downstream(dem, roughness_array, direction_array, accumulation_array, flow_lengths_array, discharge, utm_outlet):
    print("Calculating Travel Time, given a {} inch rainfall event, given in hours".format(discharge))
    traveltime_array = np.full(dem.array_size, dem.nodata_value)
    matrix_point = dem.utm_to_coord(utm_outlet)

    catchment_stack = []
    curr_pixel = [matrix_point[1], matrix_point[0], 0, 0]
    catchment_stack.append(curr_pixel)

    while len(catchment_stack) > 0:
        curr_pixel = catchment_stack[-1]

        if(curr_pixel[2] == 0):
            curr_direction = direction_array[curr_pixel[0]][curr_pixel[1]]
            #Outlet cell
            if curr_direction == 0:
                traveltime_array[curr_pixel[0]][curr_pixel[1]] = 0

            #Everything else
            else:
                index = dem.dDirection.index(curr_direction)
                length = dem.dLength[index]

                row = curr_pixel[0] + dem.dY[index]
                col = curr_pixel[1] + dem.dX[index]
                slope = (dem.array[curr_pixel[0]][curr_pixel[1]] - dem.array[row][col]) / length

                roughness_value = roughness_array[curr_pixel[0]][curr_pixel[1]]
                accumulation_value = accumulation_array[curr_pixel[0]][curr_pixel[1]]
                flow_length = flow_lengths_array[curr_pixel[0]][curr_pixel[1]]

                curr_travel_time = calc_nrcs(dem.geo_trans, roughness_value, accumulation_value, discharge, length, slope, flow_length)
                traveltime_array[curr_pixel[0]][curr_pixel[1]] = curr_pixel[3] + curr_travel_time

        if(curr_pixel[2] < 8):
            row = curr_pixel[0] + dem.dY[curr_pixel[2]]
            col = curr_pixel[1] + dem.dX[curr_pixel[2]]

            if dem.within_domain(row, col) and direction_array[row][col] == dem.dDirection_reverse[curr_pixel[2]]:
                temp_pixel = [row, col, 0, traveltime_array[curr_pixel[0]][curr_pixel[1]]]
                catchment_stack.append(temp_pixel)

            curr_pixel[2] += 1

        else:
            catchment_stack.pop()

    return traveltime_array





#travel_time
############################################################################
# Calculates the Time of Concentration for water to escape a watershed outlet
# In other words, Time of Concentration in the downstream direction
#   Determines time in hours
############################################################################

def TT_cell_to_dwnstrm(dem, traveltime_cell_array, direction_array, utm_outlet):
    print("Calculating Travel Time, given in hours")
    traveltime_array = np.full(dem.array_size, dem.nodata_value)
    matrix_point = dem.utm_to_coord(utm_outlet)

    catchment_stack = []
    curr_pixel = [matrix_point[1], matrix_point[0], 0, 0]
    catchment_stack.append(curr_pixel)

    while len(catchment_stack) > 0:
        curr_pixel = catchment_stack[-1]

        if(curr_pixel[2] == 0):

            curr_direction = direction_array[curr_pixel[0]][curr_pixel[1]]
            #Outlet cell
            if curr_direction == 0:
                traveltime_array[curr_pixel[0]][curr_pixel[1]] = 0

            #Everything else
            else:
                index = dem.dDirection.index(curr_direction)


                length = dem.dLength[index]
                row = curr_pixel[0] + dem.dY[index]
                col = curr_pixel[1] + dem.dX[index]

                curr_travel_time = traveltime_cell_array[curr_pixel[0]][curr_pixel[1]]
                traveltime_array[curr_pixel[0]][curr_pixel[1]] = curr_pixel[3] + curr_travel_time

        if(curr_pixel[2] < 8):

            row = curr_pixel[0] + dem.dY[curr_pixel[2]]
            col = curr_pixel[1] + dem.dX[curr_pixel[2]]

            if dem.within_domain(row, col) and direction_array[row][col] == dem.dDirection_reverse[curr_pixel[2]]:
                temp_pixel = [row, col, 0, traveltime_array[curr_pixel[0]][curr_pixel[1]]]
                catchment_stack.append(temp_pixel)

            curr_pixel[2] += 1

        else:
            catchment_stack.pop()

    return traveltime_array
