import math
from osgeo import gdal, osr, ogr
import numpy as np
import bisect


import time

class Raster:

    def __init__(self, dem_filename):
        dem = gdal.Open(dem_filename)
        self.geo_trans = dem.GetGeoTransform()
        self.array = dem.GetRasterBand(1).ReadAsArray()
        self.nodata_value = dem.GetRasterBand(1).GetNoDataValue()
        self.array_size = (dem.RasterYSize, dem.RasterXSize)
        self.proj_wkt = dem.GetProjection()
        dem = None


    #Within Domain
    ###########################################################################
    #   For a given pair of coordinate, check if the point is within the DEM's
    #   domain.
    #   Does not check for nodata values
    #   Returns a boolean value
    #
    ###########################################################################
    def within_domain(self, row, col):
        result = False
        if (0 <= row < self.array_size[0]) and (0 <= col < self.array_size[1]):
            result = True
        return result

    #Find Catchment
    ###########################################################################
    #   For a given outlet location, find the upstream pixels where water
    #   will flow into the outlet
    #
    ###########################################################################
    def find_catchment(self, outlet_point, direction_array, catchment_array):

        catchment_array[outlet_point[0]][outlet_point[1]] = 1
        for count in range(8):
            row = outlet_point[0] + self.dY[count]
            col = outlet_point[1] + self.dX[count]

            if self.within_domain(row, col) and direction_array[row][col] == self.dDirection_reverse[count]:
                temp_point = (row, col)
                self.find_catchment(temp_point, direction_array, catchment_array)


    #UTM to Coord
    ###########################################################################
    #   Translates at UTM coordinate to a matrix coordinate to use within the DEM
    #
    ###########################################################################
    def utm_to_coord(self, utm_point):

        matrix_pointX = int((utm_point[0] - self.geo_trans[0]) / self.geo_trans[1])
        matrix_pointY = int((utm_point[1] - self.geo_trans[3]) / self.geo_trans[5])

        coord = (matrix_pointX, matrix_pointY)

        return coord


    #Coord to UTM
    ###########################################################################
    #   Converts a matrix coordinate to a UTM coordinate
    #
    ###########################################################################
    def coord_to_utm(self, coord_point):

        utm_point_X = (coord_point[1] * self.geo_trans[1]) + self.geo_trans[0]
        utm_point_Y = (coord_point[0] * self.geo_trans[5]) + self.geo_trans[3]

        utm_point = (utm_point_X, utm_point_Y)

        return utm_point

    #Raster output
    ############################################################################
    #   Create a raster output for any of the arrays
    #
    ###########################################################################
    def output_raster(self, array, filename, **kwargs):

        if 'nodata_override' in kwargs:
            nodata_value = float(kwargs.get('nodata_override'))
        else:
            nodata_value = float(self.nodata_value)

        driver = gdal.GetDriverByName('GTiff')
        new_raster_ds = driver.Create(filename, array.shape[1], array.shape[0], 1, gdal.GDT_Float32)
        new_raster_ds.SetProjection(dem.proj_wkt)
        new_raster_ds.SetGeoTransform(self.geo_trans)
        new_raster_ds.GetRasterBand(1).SetNoDataValue(nodata_value)
        new_raster_ds.GetRasterBand(1).WriteArray(array)
        new_raster_ds.FlushCache()


    #Outlet Catchment
    ############################################################################
    #   For a generated Catchment Raster file, create a Polygon shapefile of the
    #   catchment
    #
    ###########################################################################
    @staticmethod
    def output_catchment_shp(catchment_raster, filename):
        catchment_ds = gdal.Open(catchment_raster)

        dst_layername = filename
        drv = ogr.GetDriverByName("ESRI Shapefile")
        dst_ds = drv.CreateDataSource( dst_layername )

        dst_layer = dst_ds.CreateLayer(dst_layername, srs = None )
        gdal.Polygonize( catchment_ds.GetRasterBand(1), catchment_ds.GetRasterBand(1), dst_layer, -1, [], callback=None )

        dst_ds = None
