# PhosFinder

PhosFinder is a geospatial evaluation tool, developed by Strategic Systems Engineering, that estimates nutrient transport for Canadian domains

## Functionality Overview


PhosFinder's main feature is to estimate the mass of **Nitrogen and Phosphorus** eroded and leaves the landscape by way of precipitation runoff. However, in order to make these calculations a number of datasets have to be made:


###Flow Routing
Firstly, PhosFinder has to find where precipitation is naturally routed based on elevation data. As such, PhosFinder will provide the user with native functions responsible for both **Flow Directions** and **Flow Accumulation** analysis. Both functions operate under the D8 method of routing runoff.

```python
from phosfinder import Dem

#Opens DEM as a Dem object
my_dem = Dem('filename.tif')

#Performs a depression filling functions on the Dem object's stored raster
#Depression filling based on the Wang Liu algorithm
my_dem.fill_sinks(0.01)

#Creates a Flow Direction raster using the D8 Method
direction_array = my_dem.flow_direction()

#Creates a Flow Accumulation raster based on the DEM and Flow Directions rasters
accumulation_array = my_dem.flow_accumulation(direction_array)
```

### Travel Time Calculations

Next, we'll create an array of values representing the longest upstream distance at every pixel in a given watershed within the DEM's domain. We'll call this array the **Flow Lengths** array. This array provides information that'll help PhosFinder determine how intense the runoff flows across the landscape. This functions requires the Flow Direction array and the common outlet that drains watershed.


```python
from phosFinder import NutrientMobility

outlet = [650169,5447750]
flow_length_raster = NutrientMobility.upstream_length(my_dem, direction_array, coor)
```

The **Surface Roughness** function determines the landscape's frictional force that is applied upon flowing runoff. For this function to operate, the user must provide a Flow Directions array and portion of the Annual Crop Inventory, 2019 version, that overlaps the domain.

```python
from phosfinder import Raster

aci_file = Raster('aci_2019_raster.tif')

roughness_raster = NutrientMobility.surface_roughness(my_dem, aci_file)
```

The **Travel Time** function determine the amount of time it would take for runoff to travel to the outlet if rain landed anywhere within the watershed. For this function, we need the Flow Directions, Accumulation, Flow Lengths and Surface Roughness array. In addition, we need to provide the function the watershed's outlet coordinates and the domain's precipitation depth for a 24 hour storm, on a 2 Year return, provided in inches. The travel time function will determine time in units of hours.

```python
from phosfinder import Raster

aci_file = Raster('aci_2019_raster.tif')

travel_time_array = NutrientMobility.travel_time_downstream(my_dem, roughness_raster, direction_array, accumulation_array, flow_length_raster, 123, coor)
```

### Nutrient Transport

The **Nutrient Yield** functions estimates how much nitrogen **or** phosphorus is taken from the landscape per year. Again, this function require the Annual Crop Inventory raster and the Flow Directions array.
```python
#Total Nitrogen
nitrogen_yield = NutrientMobility.tn_yield(my_dem, aci_file)

#Total Phosphorus
phosphorus_yield = NutrientMobility.tp_yield(my_dem, aci_file)
```

The **Nutrient Mass** array estimates mass of the eroded nutrient masses is taken and transported by runoff. To execute this function, the user must provide the Nutrient Yield array, the Time Travel array, the unit of time the time travel values represent, and the flow accumulation array. As a reminder, Time Travel arrays generated by PhosFinder are in units of hours.

```python
loss_array = NutrientMobility.nutrient_mass(my_dem,phosphorus_yield, travel_time_array, 'hours', accumulation_array)
```

The **Nutrient Accumulation** array determines the accumulated nutrient mass that'll be transported by runoff at each point of the watershed. Simply put, this function operates under the same logic as the Flow Accumulation array, but re-purposed for nutrient transport. As such, the function requires the Nutrient Loss array, the Flow Directions array, the watershed outlet coordinates.

```python
nutrient_acc_raster = NutrientMobility.nutrient_accumulation(my_dem,loss_array,direction_array,coor)
```
### Tests
Requires GDAL


Included test to verify validity of depression filling algorithm with a small input file. You can run the test with:
```
python3 tests/depression_fill_test.py 
```
